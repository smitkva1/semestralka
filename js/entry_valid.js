/*
javaScriptová obsluha přihlašovacího formuláře
*/

//odposlouchávače
//odeslání formuláře
document.getElementById("entry").addEventListener("submit", function(e) {controla(e)});

document.getElementById("username").addEventListener("blur", function(e) {controlUsername(e)});
document.getElementById("password").addEventListener("blur",function(e) {controlHeslo(e)});

//celková kontorla
function controla(e) {
    controlUsername(e);
    controlHeslo(e);
}

/*
kontrola username
pokud je kratší než 2 znaky vypíše chybu
*/
function controlUsername(e) {
    var jmeno = document.getElementById("username").value;
    var jmenoError = document.getElementById("usernameError");
    
    if (jmeno.length < 2) {
        jmenoError.innerHTML = '<div class="error">Krátké jméno.</div>';
        e.preventDefault();
    } else if (jmenoError.firstChild) {      
        jmenoError.removeChild(jmenoError.firstChild);
    }       
}

/*
kontrola hesla
pokud je kratší než 6 znaky vypíše chybu
*/
function controlHeslo(e) {
    var heslo = document.getElementById("password").value;
    var hesloError = document.getElementById("passwordError");

    if (heslo.length < 6) {
        hesloError.innerHTML = '<div class="error">Krátké heslo.</div>';
        e.preventDefault();
    } else if (hesloError.firstChild) {      
        hesloError.removeChild(hesloError.firstChild);
    } 
}

