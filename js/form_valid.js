/*
javaScriptová obsluha objednávajícího formuláře
*/

//odposlouchávače
//odeslání formuláře
document.getElementById("order").addEventListener("submit", function(e) {controla(e)});

//první část
document.getElementById("name").addEventListener("blur", function(e) {controlJmeno(e)});
document.getElementById("surname").addEventListener("blur",function(e) {controlPrijmeni(e)});
document.getElementById("phone").addEventListener("blur",function(e) {controlTel(e)});
document.getElementById("mail").addEventListener("blur",function(e) {controlMail(e)});

//obsluha knih
document.getElementById("K1").addEventListener("click", function(e) {smazPocet(e, "K1")});
document.getElementById("K2").addEventListener("click", function(e) {smazPocet(e, "K2")});
document.getElementById("K3").addEventListener("click", function(e) {smazPocet(e, "K3")});

document.getElementById("K1value").addEventListener("blur", function(e) {controlPocet(e, "K1")});
document.getElementById("K2value").addEventListener("blur", function(e) {controlPocet(e, "K2")});
document.getElementById("K3value").addEventListener("blur", function(e) {controlPocet(e, "K3")});

//druhá část
document.getElementById("address-text").addEventListener("blur",function(e) {controlAdresa(e)});
document.getElementById("city").addEventListener("blur",function(e) {controlMesto(e)});
document.getElementById("zip-code").addEventListener("blur",function(e) {controlPsc(e)});

//pokud označeno
document.getElementById("personal").addEventListener("click", function() {uzDobre("dopravaError")});
document.getElementById("transport").addEventListener("click", function() {uzDobre("dopravaError")});

function controla(e) {
    //kontrola aby byla vybrána doprava
    var trancheck = document.getElementById("transport").checked;
    var percheck = document.getElementById("personal").checked;

    var doprava = trancheck || percheck;
    var tranError = document.getElementById("dopravaError");

    if (!doprava) {
        tranError.innerHTML = '<div class="error">Je třeba vybrat jednu z variant.</div>';
        e.preventDefault();
    } else if (tranError.firstChild) {      
        tranError.removeChild(tranError.firstChild);
    } 
    
    //kontrola druhé části pokud je zaškrtnutá doprava
    if (trancheck) {
        controlAdresa(e);
        controlMesto(e);
        controlPsc(e);
    }

    //obstarání výpisu chyby u souhlasu
    var agreeCheck = document.getElementById("agree").checked;
    var agreeError = document.getElementById("agreeJs");

    if (!agreeCheck) {
        agreeError.innerHTML = '<span class="error">Není vyplněno.</span>';
        e.preventDefault();   
    } else if (agreeError.firstChild) {      
        agreeError.removeChild(agreeError.firstChild);
    } 

    //kontorlo označení alespoň jedné knihy
    var K1check = document.getElementById("K1").checked;
    var K2check = document.getElementById("K2").checked;
    var K3check = document.getElementById("K3").checked;
    
    var checked = true;
    if (K1check) {
        controlPocet(e, "K1");
        checked = false;
    }
    if (K2check) {
        controlPocet(e, "K2");
        checked = false;
    }
    if (K3check) {
        controlPocet(e, "K3");
        checked = false;
    }

    var knihyError = document.getElementById("knihyError");
    if (checked) {
        knihyError.innerHTML = '<div class="error">Je třeba vybrat alespoň jednu knihu.</div>';
        e.preventDefault();
    } else if (knihyError.firstChild) {      
        knihyError.removeChild(knihyError.firstChild);
    }
    
    //kontrola první části
    controlJmeno(e);
    controlPrijmeni(e);
    controlTel(e);
    controlMail(e);
    controlIsKniha (e); 
}

/*
kontrola jmena
pokud je kratší než 2 znaky vypíše chybu
*/
function controlJmeno(e) {
    var jmenoCon = document.getElementById("name").value;
    var jmeno = document.getElementById("jmenoError");
    
    if (jmenoCon.length < 2) {
        jmeno.innerHTML = '<div class="error">Příliš krátké jméno.</div>';
        e.preventDefault();
    } else if (jmeno.firstChild) {      
        jmeno.removeChild(jmeno.firstChild);
    }       
}

/*
kontrola příjmení
pokud je kratší než 2 znaky vypíše chybu
*/
function controlPrijmeni(e) {
    var prijmeniCon = document.getElementById("surname").value;
    var prijmeni = document.getElementById("prijmeniError");

    if (prijmeniCon.length < 2) {
        prijmeni.innerHTML = '<div class="error">Příliš krátké příjmení.</div>';
        e.preventDefault();
    } else if (prijmeni.firstChild) {      
        prijmeni.removeChild(prijmeni.firstChild);
    } 
}

/*
kontrola telefonu
pokud není číslo a není dlouhé 9 vypíše chybu
*/
function controlTel(e) {
    var tel = document.getElementById("phone").value;
    var telError = document.getElementById("telError");
    var telN = isNaN(Number(tel));

    if(telN || (tel.length != 9)) {
        telError.innerHTML = '<div class="error">Telefon neobsahuje pouze 9 číslic.</div>';
        e.preventDefault();
    } else if(telError.firstChild) {
        telError.removeChild(telError.firstChild);
    }
}

/*
kontrola mailu
pokud je kratší než 5 znaků, poslední . je před @,
. je poslední znak, @ je první znak a @ a . jsou vedle sebe vypíše chybu
*/
function controlMail(e) {
    var mail = document.getElementById("mail").value;
    var mailError = document.getElementById("mailError");
    var at = mail.indexOf("@");
    var dot = mail.lastIndexOf(".");
    
    if (!(mail.length >= 5) || at <= 0 || dot == -1 || at > dot || 
        (dot - at) == 1 || dot == (mail.length-1))  {
        mailError.innerHTML = '<div class="error">Chybně zadaný mail.</div>';
        e.preventDefault();
    } else if(mailError.firstChild) {
        mailError.removeChild(mailError.firstChild);
    }
}

/*
kontrola počtu knih
pokud je záporné nebo žádné vypíše chybu
*/
function controlPocet(e, id) {
    var idvalue = id + "value";
    var value =  document.getElementById(idvalue).value;
    var ischeck = document.getElementById(id).checked;
    var valueError = document.getElementById("knihyError");

    if ((value < 1 || !value) && ischeck) {
        valueError.innerHTML = '<div class="error">Je zadat kladný počet knih.</div>';
        e.preventDefault();
    } else if(valueError.firstChild) {
        valueError.removeChild(valueError.firstChild);
        vypocitejCenu(id);
    } else {
        vypocitejCenu(id);
    }
}

/*
kontrola zda nějaká kniha je oznacena
projde všechny tři hodnoty a pokud ani jedna není označena tak vyskočí chyba
*/
function controlIsKniha (e) {
    var knihyOk = false;
    var valueError = document.getElementById("knihyError");

    for (i = 1; i <= 3; i++) {
        var idvalue = "K" + i + "value";
        var value =  document.getElementById(idvalue).value;
        if (value > 0) {
            knihyOk = true;
        }
    }
    if (!knihyOk) {
        valueError.innerHTML = '<div class="error">Je třeba vybrat alespoň jednu knihu.</div>';
        e.preventDefault();
    }
}

//smaze počet knih, když se odznačí
function smazPocet(e, id) {
    var idvalue = id + "value";
    var ischeck = document.getElementById(id).checked;
    if (!ischeck) {
        document.getElementById(idvalue).value = "";
        controlPocet(e, id);
    } 
}

/*
kontrola adresy
pokud je kratší než 3 znaky, nenachází se tam mezera a nakonci je číslo vypíše chybu
*/
function controlAdresa(e) {
    var adresa = document.getElementById("address-text").value;
    var adresaError = document.getElementById("adresaError");
    var space  = adresa.indexOf(" ");
    var indexy = space < 1 || space == (adresa.length-1);
    var pom = adresa.lastIndexOf(" ") + 1; 

    var isnumber = true;
    while (pom < adresa.length) {
        isnumber = (isNaN(Number(adresa[pom])) || adresa[pom] == "/") && isnumber;
        pom += 1;
    }

    if (adresa.length < 3 || indexy || isnumber) {
        adresaError.innerHTML = '<div class="error">Adresa není ve formátu: název mezera číslo popisné.</div>';
        e.preventDefault();
    } else if(adresaError.firstChild) {
        adresaError.removeChild(adresaError.firstChild);
    } 
}

/*
kontrola města
pokud je kratší než 2 znaky vypíše chybu
*/
function controlMesto(e) {
    var mesto = document.getElementById("city").value;
    var mestoError = document.getElementById("mestoError");

    if (mesto.length < 2) {
        mestoError.innerHTML = '<div class="error">Nejedná se o město.</div>';
        e.preventDefault();
    } else if(mestoError.firstChild) {
        mestoError.removeChild(mestoError.firstChild);
    }         
}

/*
kontrola psč
pokud pokud se nejedná o číslo a není dlouhé 5 znaků vypíše chybu
*/
function controlPsc(e) {
    var psc = document.getElementById("zip-code").value;
    var pscError = document.getElementById("pscError");
    var isnumber = isNaN(Number(psc));

    if (psc.length != 5 || isnumber) {
        pscError.innerHTML = '<div class="error">PSČ neobasahuje pouze 5 číslic.</div>';
        e.preventDefault();
    } else if(pscError.firstChild) {
        pscError.removeChild(pscError.firstChild);
    }  
}

//pokud již označeno je potřeba smazat chybovou hlášku
function uzDobre(id) {
    var target = document.getElementById(id);
    if(target.firstChild) {
        target.removeChild(target.firstChild);
    } 
}

//vypočítá momentální celkovou cenu objednávky
function vypocitejCenu() {
    var ceny = [230, 200, 150];
    var celkovaCena = 0;
    for (i = 1; i <= 3; i++) {
        var idvalue = "K" + i + "value";
        var value =  document.getElementById(idvalue).value;
        if (value > -1) {
            celkovaCena += value * ceny[i-1]; 
        }
    }    
    document.getElementById("celkovaCena").textContent = celkovaCena;
}