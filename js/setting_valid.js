/*
javaScriptová obsluha formulář nastavení v edministrativě
*/

//odposlouchávače
//odeslání formuláře
document.getElementById("pocetRadku").addEventListener("submit", function(e) {controla(e)});

document.getElementById("number").addEventListener("blur", function(e) {controlNumber(e)});

//celková kontorla
function controla(e) {
    controlNumber(e);
}

/*
kontrola username
pokud je kratší než 1 znaky vypíše chybu
*/
function controlNumber(e) {
    var number = document.getElementById("number").value;
    var numberError = document.getElementById("numberError");
    
    if (number < 1) {
        numberError.innerHTML = '<div class="error">Nemůže být nekladný počet.</div>';
        e.preventDefault();
    } else if (numberError.firstChild) {      
        numberError.removeChild(numberError.firstChild);
    }       
}