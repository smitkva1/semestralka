﻿<?php 
    include("helper/ses.php");
    
    /*
    zde je předpřipravená stránka pro výpis a obhospodařování knihovny článků
    */
    //zahlaví
    include("htmlKomponenty/zahlavi.php");

    //tělo
    include("view/clanky.php");

    //zápatí
    include("htmlKomponenty/zapati.php"); 
?>

