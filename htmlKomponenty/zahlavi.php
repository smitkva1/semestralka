<!DOCTYPE html>
<html lang='cs'>
    <head>
        <meta charset="UTF-8">
        <title>Kout historických kuriozit</title>

        <?php 
            //zjištění aktuálního skinu
            if (isset($_COOKIE["skin"])){
                $skin = htmlspecialchars($_COOKIE["skin"]);
            } else {
                $skin = "style1";
            }
        ?>
        <!--připojení kaskádových stylů-->
        <link rel="stylesheet" type="text/css" href="css/<?php echo $skin; ?>/layout.css">
        <link rel="stylesheet" type="text/css" href="css/<?php echo $skin; ?>/styles.css">
        <!-- styl tisku-->
        <link rel="stylesheet" type="text/css" href="css/layout-print.css" media="print"> 
        <link rel="stylesheet" type="text/css" href="css/styles-print.css" media="print">
        <!-- pro oba skiny -->
        <link rel="stylesheet" type="text/css" href="css/styles.css">
    </head>
    <body>
        <div class="grid-container">     
            <div class="hlavicka"> 
                <div class="hlavicka-text"> Kout historických kuriozit </div>
                <a class="logo-a" href="index.php">
                <img class="logo" src="pictures/logo.png" alt="logo">  
                </a>
                <div class="hlavicka-text"> Ab Anno Domini 2013 </div>
            </div> 
            <!-- Hlavní menu-->
            <div class="menu">
            <nav>
                <ul class="list-menu">   
                    <li class="list">
                        <a href="index.php" class="item"> Úvod </a>
                    </li>   
                    <li class="list">
                        <a href="order_form.php" class="item"> Objednávka </a>
                    </li>
                    <li class="list">
                        <a href="list_article.php" class="item"> Články </a>
                    </li>
                    <li class="list">
                        <a href="kontakt.php" class="item"> Kontakt </a>
                    </li>                               
                </ul>
                <?php 
                    //obsluha administrativního menu druhého skinu
                    if (isset($_SESSION)) {
                        if (isset($_COOKIE["skin"])) {
                            $skin = htmlspecialchars($_COOKIE["skin"]);
                        }

                        if (isset($_SESSION["login"]) && $skin == "style2") {
                            //admin lišta
                            include("htmlKomponenty/admin_lista_style2.html");
                        }
                    } 
                    ?>       
            </nav>
            </div>
            <!-- začátek obsahu-->
            <div class="main">