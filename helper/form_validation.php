<?php
    /*
    pomocné funkce pro kontrolu formuláře
    */

    /*
    kontrola jmena
    pokud je roven nebo delší 2 znaky je v pořádku
    */
    function valName ($name) {       
        return strlen($name) >= 2;
    }

    /*
    kontrola příjmení
    pokud je roven nebo delší 2 znaky je v pořádku
    */
    function valSurname ($surname) {
        return strlen($surname) >= 2;
    }

    /*
    kontrola telefonu
    pokud je to kladné číslo a je dlouhé 9 znaků je v pořádku
    vymaže mezery,  pomlčky a plus
    */
    function valTel ($tel){
        $tel = str_replace(" ", "", $tel);
        $tel = str_replace("-", "", $tel);
        $tel = str_replace("+", "", $tel);
        $isnumber = is_numeric($tel);
        $tel = (int) $tel;
        $isplus = $tel > 0;
        $telS = (string) $tel;
        $isnine = strlen($telS) == 9;
        return $isnumber && $isnine && $isplus;
    }

    /*
    kontrola mailu
    pokud je roven nebo delší 5 znaků, obsahuje . a @,
    nejsou vedle sebe . a  @, není @ první a poslední . posledná
    je v pořádku
    */
    function valMail ($mail){
        $isat = strpos($mail, "@");
        $isdot = strpos($mail, ".");
        $islong = strlen($mail) >= 5;
        $indexy = ($isat < $isdot || ($isdot - $isat) > 1 || 
        $isat > 0 || $isdot < ($islong-1));
        return $isat && $isdot && $islong && $indexy;
    }

    /*
    kontrola počtu knížek
    pokud to kladný počet je v pořádku
    */
    function vyresK ($pocetK) {
        return $pocetK > 0;
    }

    /*
    kontrola adresy
    pokud je roven nebo delší 3 znaky, obsahuje mezeru
    a na konci je číslo je v pořádku
    */
    function valAddress ($address){
        $islong = strlen($address) >= 3;
        $str = strtok($address, ' ');
        $val = is_numeric($str);
        return !$val && $islong;
    }
    
    /*
    kontrola města
    pokud je roven nebo delší 2 znaky je v pořádku
    */
    function valCity ($city){
        return strlen($city) >= 2;
    }
    
    /*
    kontrola PSČ
    pokud je roven 5 znaků a je to kladné číslo je v pořádku
    vymaže mezery a pomlčky
    */
    function valZip ($zip){
        $zip = str_replace(" ", "", $zip);
        $zip = str_replace("-", "", $zip);
        $isnumber = is_numeric($zip);
        $zip = (int) $zip;
        $isplus = $zip > 0;
        $zipS = (string) $zip;
        $isfive = strlen($zipS) == 5;
        return $isnumber && $isfive;
    }

        /*
    kontrola hesla
    pokud je roven nebo delší 6 znaků je v pořádku
    */
    function valHeslo ($password) {       
        return strlen($password) >= 6;
    }
?>