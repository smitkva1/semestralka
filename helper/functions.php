<?php
    /*
    pomocné funkce pro obecné použití
    */
    
    //udržení chtěných knih
    function vyresKnihy($pole) {
        $knihy = ["", "", ""];
        foreach ($pole as $value) {
            $knihy[$value] = "checked";
            
        }
        return $knihy;
    }

    //ošetření vstupů proti útokům
    function osetri($data) {
        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);
        return $data;
    }

    //funkce pro přípravu žádosti pro databázi při řazení
    function sqlPrikazVytvor($data, $prikaz) {
        if (isset($data["sortUp"])) {
            $order_by = osetri($data["sortUp"]);
            $sql = "SELECT * FROM objednavky ".$prikaz." ORDER BY ". $order_by ." ASC";
            return $sql;
        } elseif (isset($data["sortDown"])) {
            $order_by = osetri($data["sortDown"]);
            $sql = "SELECT * FROM objednavky ".$prikaz." ORDER BY ". $order_by ." DESC";
            return $sql;
        } else {
            $sql = "SELECT * FROM objednavky ".$prikaz." ORDER BY order_id DESC";
            return $sql;
        }
    }

    //vyvolá z cookie skin a pokud není žádný nastaví první
    function zjistiSkin() {
        if (isset($_COOKIE["skin"])) {
            return  htmlspecialchars($_COOKIE["skin"]);
        } else {
            return  "style1";
        }
    }
?>