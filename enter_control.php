<?php 
include("helper/ses.php");

/*
zde se provádí obsluha přihlašovacího formuláře
*/
include("helper/functions.php");
include("helper/form_validation.php");

$username = "";
$login = FALSE;
$send = FALSE;

/*pokud je odeslán ošetříse hodnoty a vezmou se na kontolu s databází
jen tehdy když nejsou moc krátké */
if (count($_POST) > 0) {
    $send = TRUE;
    $username = osetri($_POST["username"]);
    $heslo = osetri($_POST["password"]);

    $valUsername = valName($username); 
    $valHeslo = valHeslo($heslo);

    if ($valUsername && $valHeslo) {
        $hesloHash = sha1($heslo."s15w3wfHG8KW");
        include("database/enter.php");
    }
}

//pokud je všw v pořádku dostávám se do administrace jinak zůstávám ve formuláři
if ($login) {
    $_SESSION["login"] = TRUE;
    header('Location: control_manager.php');
} else {
    //zahlaví
    include("htmlKomponenty/zahlavi.php");
    //tělo
    include("view/enter_manager.php");
    //zápatí
    include("htmlKomponenty/zapati.php"); 
}
?>