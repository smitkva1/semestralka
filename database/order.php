<?php           
    /*
    zde se přihlašuji k databázi pomocí PDO
    podle proměné $edit v které je uloženo zda se upravuje nebo vytváří
    se buď otevře dtabulka s objednávkami pro zápis nebo přeuložení podle id
    */
                
    $user = "erakles";
    $password = "NXUBpjGu1Taj00qo";
    $ceny = [230, 200, 150];

    try {
        //připojení k databázi
        $link = new PDO('mysql:host=localhost;dbname=semestralka',
        $user, $password, array(
        PDO::ATTR_PERSISTENT => true
        ));

        $link->beginTransaction();
        
        if ($edit) { 
            //předdefinování a příprava hodnot
            $stmt = $link->prepare("UPDATE objednavky SET 
            jmeno = :jmeno,
            prijmeni = :prijmeni, 
            telefon = :telefon, 
            mail = :mail, 
            doprava = :doprava, 
            adresa = :adresa, 
            mesto = :mesto, 
            psc = :psc, 
            poznamka = :poznamka, 
            kniha1 = :kniha1,
            kniha2 = :kniha2, 
            kniha3 = :kniha3,
            celkovacena = :celkovacena
            WHERE order_id=".$id);

        } else {
            //předdefinování a příprava hodnot
            $stmt = $link->prepare("INSERT INTO objednavky (jmeno,
            prijmeni, telefon, mail, doprava, adresa, mesto, psc, poznamka, kniha1,
            kniha2, kniha3, celkovacena,  datum) 
            VALUES(:jmeno, :prijmeni, :telefon, :mail, :doprava, :adresa, :mesto, :psc, 
            :poznamka, :kniha1, :kniha2, :kniha3, :celkovacena, :datum)");
            
            $stmt->bindParam(':datum', $date);
    }
    
    $stmt->bindParam(':jmeno', $name);
    $stmt->bindParam(':prijmeni', $surname);
    $stmt->bindParam(':telefon', $phone);
    $stmt->bindParam(':mail', $email);
    $stmt->bindParam(':doprava', $delivery);
    $stmt->bindParam(':adresa', $address);
    $stmt->bindParam(':mesto', $city);
    $stmt->bindParam(':psc', $zip);
    $stmt->bindParam(':poznamka', $notepad);
    $stmt->bindParam(':kniha1', $book1);
    $stmt->bindParam(':kniha2', $book2);
    $stmt->bindParam(':kniha3', $book3);
    $stmt->bindParam(':celkovacena', $valueAll);
        
    $name = $jmeno;
    $surname = $prijmeni;
    $phone = $tel;
    $email = $mail;

    if ($checkT) {
        $delivery = '1';
        $address = $adresa;
        $city = $mesto;
        $zip = $psc;
    } else {
        $delivery = '0';
        $address = '';
        $city = '';
        $zip = '';
    }

    $notepad = $note;
    $book1 = $knihyValue[0];
    $book2 = $knihyValue[1];
    $book3 = $knihyValue[2];
    $valueAll = ($book1 * $ceny[0]) + ($book2 * $ceny[1]) + ($book3 * $ceny[2]);
    $date = date("Y-m-d h:i:sa");
    
    $stmt->execute();

    $link->commit();

    } catch(Exception $e) {
    echo 'Zpráva: ' .$e->getMessage();
    }
?>