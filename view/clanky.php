<!-- 
    Tato stránka není zatím dodělaná
    jedná se o přípravu pro knihovnu článků
    jsou zde jen pro ilustraci, ve výsledku se budou volat z databáze 
-->
<div class="content ar-content">
    <header>
        <h1>Seznam článků</h1>
    </header>
    
    <!-- clánek -->
    <div id="1" class="article">
        <div class="box-header">
            <h3 class="box-name">Article1</h3>
        </div>
        <div class="box-body ar-body">
            <p class="box-text">
                Text Text Text Text Text Text Text Text
                Text Text Text Text Text Text Text Text Text Text Text Text
                Text Text Text Text Text Text Text Text...
            </p>
        </div>
        <div class="box-footer ar-footer ">
        </div>
    </div>

    <!-- clánek -->
    <div id="2" class="article">
        <div class="box-header">
            <h3 class="box-name">Article2</h3>
        </div>
        <div class="box-body">
            <p class="box-text ar-body">
                Text Text Text Text Text Text Text Text
                Text Text Text Text Text Text Text Text Text Text Text Text
                Text Text Text Text Text Text Text Text...
            </p>  
        </div>
        <div class="box-footer ar-footer">
        </div>
    </div>

    <!-- clánek -->
    <div id="3" class="article">
        <div class="box-header">
            <h3 class="box-name">Article3</h3>
        </div>
        <div class="box-body">
            <p class="box-text ar-body">
                Text Text Text Text Text Text Text Text
                Text Text Text Text Text Text Text Text Text Text Text Text
                Text Text Text Text Text Text Text Text...
            </p>
        </div>
        <div class="box-footer ar-footer">
        </div>
    </div>

    <!-- clánek -->
    <div id="4" class="article">
        <div class="box-header">
            <h3 class="box-name">Article4</h3>
        </div>
        <div class="box-body">
            <p class="box-text ar-body">
                Text Text Text Text Text Text Text Text
                Text Text Text Text Text Text Text Text Text Text Text Text
                Text Text Text Text Text Text Text Text...
            </p>
        </div>
        <div class="box-footer ar-footer">
        </div>
    </div>

    <!-- clánek -->
    <div id="5" class="article">
        <div class="box-header">
            <h3 class="box-name">Article5</h3>
        </div>
        <div class="box-body">
            <p class="box-text ar-body">
                Text Text Text Text Text Text Text Text
                Text Text Text Text Text Text Text Text Text Text Text Text
                Text Text Text Text Text Text Text Text...
            </p>
        </div>
        <div class="box-footer ar-footer">
        </div>
    </div>

    <!-- clánek -->
    <div id="6" class="article">
        <div class="box-header">
            <h3 class="box-name">Article6</h3>
        </div>
        <div class="box-body">
            <p class="box-text ar-body">
                Text Text Text Text Text Text Text Text
                Text Text Text Text Text Text Text Text Text Text Text Text
                Text Text Text Text Text Text Text Text...
            </p>
        </div>
        <div class="box-footer ar-footer">
        </div>
    </div>

    <!-- clánek -->
    <div id="7" class="article">
        <div class="box-header">
            <h3 class="box-name">Article7</h3>
        </div>
        <div class="box-body">
            <p class="box-text ar-body">
                Text Text Text Text Text Text Text Text
                Text Text Text Text Text Text Text Text Text Text Text Text
                Text Text Text Text Text Text Text Text...
            </p>
        </div>
        <div class="box-footer ar-footer">
        </div>
    </div>

    <!-- clánek -->
    <div id="8" class="article">
        <div class="box-header">
            <h3 class="box-name">Article8</h3>
        </div>
        <div class="box-body">
            <p class="box-text ar-body">
                Text Text Text Text Text Text Text Text
                Text Text Text Text Text Text Text Text Text Text Text Text
                Text Text Text Text Text Text Text Text...
            </p>
        </div>
        <div class="box-footer ar-footer">
        </div>
    </div>
</div>