<!-- Toto je obsahová stránka z administrace -> nastavení 
mění se zde skiny a upravuje počet zobrazených položek na jedné stránce v tabulce objednávek-->
<div class="content">
<?php 
    /*
    obsluha zpráv se ztrany serveru ohledne 
    uskutečnených nebo neuskutečnených akcí
    */
    $class = ($message) ? "message" : "hide"; 
?>        
    <!--Výpis hlášky serveru -->
    <div class="<?php echo $class?>"> 
        <?php echo $message; ?>
    </div>

    <header>
        <h1>Nastavení</h1>
    </header> 

    <!--výměna skinů -->
    <div class="form-table">
        <form method="GET"> 
        <label class="form-name form-setting" for="skin"> Změna skinu: </label>
            <select class="setting-input" id="skin" name="skin">
                <option value="style1">Skin 1</option>
                <option value="style2">Skin 2</option>
            </select>
            <input class="submit sub-setting" type="submit" value="potvrď">
        </form>
    </div>
    
    <!--Změna poctu zobrazených položek na jedné stránce -->
    <div class="form-table">
        <form id="pocetRadku" method="GET">
            <label class="form-name form-setting" for="number"> Změna počtu řádků tabulky: </label> 
            <input id="number" class="setting-input" type="number" name="number" value="<?php echo $number;?>" placeholder="<?php echo $_COOKIE["number"]?>">
            <input class="submit sub-setting" type="submit" value="potvrď">
            <span id="numberError">
                <?php      
                    if (!$valNumber) {
                        echo '<div class="error">Nemůže být nekladný počet.</div>';
                    } 
                ?>
            </span>
        </form>
    </div>           
</div>

<!--javaScriptová obsluha formulářů na této stránce -->
<script>
    <?php include("js/setting_valid.js");?>
</script>