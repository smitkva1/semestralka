﻿<!-- zde je formulář pro přihlášení do administrace --> 
<div class="form">
    <header>
        <h1>Příhlášení</h1>
    </header>
    <?php 
        //vypsání chyby když se nepovede přihlásit
        if ($send) {
        echo '<div class="error">Špatně zadané jméno nebo heslo.</div>';
        } 
    ?>
    <form id="entry" class="order-form" method="POST">
        <fieldset class="form-table">
                <div>
                    <label class="form-name" for="username"> Jméno: </label>
                    <input type="text" id="username" name="username" value="<?php echo $username ?>" required>             
                    <span id="usernameError">    
                        <?php      
                            //vypsání serverové chyby krátkého jména jako dvojí kontorola
                            if ((count($_POST) > 0) && !$valUsername) {
                                echo '<div class="error">Krátké jméno.</div>';
                            } 
                        ?>
                    </span>
                </div>
                <div>
                    <label class="form-name" for="password"> Heslo: </label>
                    <input type="password" id="password" name="password" required>
                    <span id="passwordError">    
                        <?php      
                            //vypsání serverové chyby krátkého hesla jako dvojí kontorola
                            if ((count($_POST) > 0) && !$valHeslo) {
                                echo '<div class="error">Krátké heslo</div>';
                            } 
                        ?>
                    </span>
                </div>                              
            <input class="submit" type="submit" value="Přihlásit">
        </fieldset>                 
    </form>
</div>
<!-- javaScriptová obsluha přihlašovacího formuláře --> 
<script>
    <?php include("js/entry_valid.js");?>
</script>
