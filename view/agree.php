<!-- 
    část formuláře, kde se odsouhlasují podmínky o uschově dat (Zatím není zavedeno)
    zvlášť je z důvodu možného použití i v dalších případech
-->
<div class="agree">
    <div class="agree-box">
        <input class="small" type="checkbox" name="agree" id="agree" required>
        <label for="agree"> Souhlasím s </label> 
        <a href="#" class="agree-href">podmínkami.</a>
        <div id="agreeJs" class="line">
            <?php      
                //vypání chyby ze serveru
                if (!isset($_POST["agree"]) && count($_POST) > 0) {
                    echo '<span class="error">Není vyplněno.</span>';
                } 
            ?>
        </div>
    </div>
</div>