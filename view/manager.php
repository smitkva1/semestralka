﻿<!--Zde se je stránka vypisující objednávky do tabulky -->
<?php 
    /*
    obsluha zpráv se ztrany serveru ohledne 
    uskutečnených nebo neuskutečnených akcí
    */
    $class = ($message) ? "message" : "hide"; 
?>        
<div class="content manager-content">
    <!--Výpis hlášky serveru -->
    <div class="<?php echo $class?>"> 
        <?php echo $message; ?>
    </div>

    <header>
        <h1>Administrace</h1>
    </header>              
    
    <form class="filter-form" method="POST">
        <div class="filter-box">
            <input type="text" placeholder="Jméno:" name="jmeno" value="<?php echo $jmeno;?>">
        </div>
        <div class="filter-box">
            <input type="text" placeholder="Příjmení:" name="prijmeni" value="<?php echo $prijmeni;?>">
        </div>
        <div class="filter-box">
            <input type="text" placeholder="Telefon:" name="telefon" value="<?php echo $telefon;?>">
        </div>
        <div class="filter-box">
            <input type="text" placeholder="Mail:" name="mail" value="<?php echo $mail;?>">
        </div>
        <div class="filter-box">
            <select class="select-filter" name="doprava">
                <option value="-1">Doprava:</option>
                <option value="0" <?php echo $dopravaO;?>>Osobní</option>
                <option value="1" <?php echo $dopravaD;?>>Doprava</option>
            </select>
        </div>
        <div class="filter-box">
        <select class="select-filter" name="poznamka">
                <option value="-1">Poznámka:</option>
                <option value="1" <?php echo $poznamkaJ;?>>je</option>
                <option value="0" <?php echo $poznamkaN;?>>není</option>
            </select>
        </div>
        <div class="filter-box">
            <label for="od"> Od: </label>
            <input type="date" id="od" name="od" value="<?php echo $od;?>">
            <label for="do"> Do: </label>
            <input type="date" id="do" name="do" value="<?php echo $do;?>">
        </div>
        <div class="filter-box">
            <input class="submit filter-submit" type="submit" value="Vyhledej">
        </div>
    </form>

    <table id="manager-table"> 
        <!-- Hlavička tabulky s čadícíma ikonama-->
        <thead>
            <tr>
                <th>
                    Jméno
                    <a class="sort" href="control_manager.php?sortUp=jmeno">
                    <img class="icon-sort" src="pictures/icons/up.ico" alt="up">
                    </a>
                    <a class="sort" href="control_manager.php?sortDown=jmeno">
                    <img class="icon-sort" src="pictures/icons/down.ico" alt="down">
                    </a>         
                </th>
                <th>
                    Příjmení
                    <a class="sort" href="control_manager.php?sortUp=prijmeni">
                    <img class="icon-sort" src="pictures/icons/up.ico" alt="up">
                    </a>
                    <a class="sort" href="control_manager.php?sortDown=prijmeni">
                    <img class="icon-sort" src="pictures/icons/down.ico" alt="down">
                    </a>  
                </th>
                <th>
                    Telefon
                    <a class="sort" href="control_manager.php?sortUp=telefon">
                    <img class="icon-sort" src="pictures/icons/up.ico" alt="up">
                    </a>
                    <a class="sort" href="control_manager.php?sortDown=telefon">
                    <img class="icon-sort" src="pictures/icons/down.ico" alt="down">
                    </a>  
                </th>
                <th>
                    Mail
                    <a class="sort" href="control_manager.php?sortUp=mail">
                    <img class="icon-sort" src="pictures/icons/up.ico" alt="up">
                    </a>
                    <a class="sort" href="control_manager.php?sortDown=mail">
                    <img class="icon-sort" src="pictures/icons/down.ico" alt="down">
                    </a>  
                </th>
                <th>
                    Doprava
                    <a class="sort" href="control_manager.php?sortUp=doprava">
                    <img class="icon-sort" src="pictures/icons/up.ico" alt="up">
                    </a>
                    <a class="sort" href="control_manager.php?sortDown=doprava">
                    <img class="icon-sort" src="pictures/icons/down.ico" alt="down">
                    </a>  
                </th>
                <th>
                    Objednávka
                    <a class="sort" href="control_manager.php?sortUp=celkovacena">
                    <img class="icon-sort" src="pictures/icons/up.ico" alt="up">
                    </a>
                    <a class="sort" href="control_manager.php?sortDown=celkovacena">
                    <img class="icon-sort" src="pictures/icons/down.ico" alt="down">
                    </a>  
                </th>
                <th>
                    Poznámka
                    <a class="sort" href="control_manager.php?sortUp=poznamka">
                    <img class="icon-sort" src="pictures/icons/up.ico" alt="up">
                    </a>
                    <a class="sort" href="control_manager.php?sortDown=poznamka">
                    <img class="icon-sort" src="pictures/icons/down.ico" alt="down">
                    </a>  
                </th>
                <th>
                    Datum
                    <a class="sort" href="control_manager.php?sortUp=datum">
                    <img class="icon-sort" src="pictures/icons/up.ico" alt="up">
                    </a>
                    <a class="sort" href="control_manager.php?sortDown=datum">
                    <img class="icon-sort" src="pictures/icons/down.ico" alt="down">
                    </a>  
                </th>
                <th>
                    Úprava
                    <a class="sort" href="control_manager.php?refresh">
                    <img class="icon-sort" src="pictures/icons/refresh.ico" alt="refresh">
                    </a>  
                </th>
            </tr>
        </thead>
        <tbody>
            <?php
            //zde se vypisují z databáze objednávky
            if ($result) {
                while($row = mysqli_fetch_assoc($result)) {
                    if ($row["doprava"] == "0") {
                        $doprava = "osobní";
                    } else {
                        $doprava = "doprava";
                    }

                    if ($row["poznamka"] == "") {
                        $poznamka = "";
                    } else {
                        $poznamka = "je";
                    }

                    echo '<tr>
                        <td>
                            '.$row["jmeno"].'       
                        </td>
                        <td>
                            '.$row["prijmeni"].'
                        </td>
                        <td>
                            '.$row["telefon"].'
                        </td>
                        <td>
                            '.$row["mail"].'
                        </td>
                        <td>
                            '.$doprava.'
                        </td>
                        <td>
                            '.$row["celkovacena"].'
                        </td>
                        <td>
                            '.$poznamka.'
                        </td>
                        <td>
                            '.$row["datum"].'
                        </td>
                        <td>
                            <a class="icon" href="control_manager.php?edit='.$row["order_id"].'">
                            <img  class="icon" src="pictures/icons/edit.ico" alt="edit">
                            </a>    
                            <a class="icon" href="control_manager.php?delete='.$row["order_id"].'">
                            <img  class="icon" src="pictures/icons/delete.ico" alt="delete"> 
                            </a>
                        </td>
                    </tr>';
                }
                mysqli_free_result($result);
            }
        ?>
        </tbody>
    </table>  

    <!-- lišta pro přeskakování mezi stránkami-->
    <div class="bar">
        <div class="bar-helper">
            <!-- na první -->
            <div class="bar-box">
                <a class="bar-href" href="control_manager.php?list=first"> 
                    <div class="bar-number">&laquo;</div>
                </a>
            </div>
            <!-- na predchozí -->
            <div class="bar-box">
                <a class="bar-href" href="control_manager.php?list=back"> 
                    <div class="bar-number">&lsaquo;</div>
                </a>
            </div>
            <!-- vypsané všechny čísla stránek které existují -->
            <?php
            $zbytek = $count % $naStranku;
            $pocetStranek = (($count - $zbytek)/$naStranku);
            $pocetStranek = ($zbytek > 0) ? $pocetStranek + 1 : $pocetStranek;
            $pocetStranek = intval($pocetStranek);

            for ($i = 1; $i <= $pocetStranek; $i++) { 
                $ted = ($i == (intval($odPoctu/$naStranku)+1) ) ? "bar-ted" : ""; 
                echo '<div class="bar-box">
                <a class="bar-href" href="control_manager.php?list='.$i.'"> 
                <div class="bar-number '.$ted.'">'.$i.'</div>
                </a>
                </div>';
            }
            ?> 
            <!-- na další -->
            <div class="bar-box">
                <a class="bar-href" href="control_manager.php?list=next"> 
                    <div class="bar-number">&rsaquo;</div>
                </a>
            </div>
            <!-- na poslední -->
            <div class="bar-box">
                <a class="bar-href" href="control_manager.php?list=last"> 
                    <div class="bar-number">&raquo;</div>
                </a>
            </div>
        </div>     
    </div>
</div>
