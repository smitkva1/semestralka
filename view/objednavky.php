<!-- Zde je formulář na vytváření objednávek -->
<div class="form">
    <header>
        <h1>Objednávací formulář</h1>
    </header>

    <form class="order-form" id="order" method="POST">
        
        <!-- první část jsou jen základní údaje -->
        <fieldset class="form-table">
                <div>
                        <label class="form-name" for="name"> Jméno*: </label>
                        <input type="text" id="name" name="name" value="<?php echo $jmeno ?>" required>
                    <span id="jmenoError">    
                        <?php      
                            if ((count($_POST) > 0) && !$valJmeno) {
                                echo '<div class="error">Příliš krátké jméno.</div>';
                            } 
                        ?>
                    </span>    
                </div>
                <div>
                        <label class="form-name" for="surname"> Příjmení*: </label>
                        <input type="text" id="surname" name="surname" value="<?php echo $prijmeni ?>" required>
                    <span id="prijmeniError">    
                        <?php      
                            if ((count($_POST) > 0) && !$valPrijmeni) {
                                echo '<div class="error">Příliš krátké příjmení.</div>';
                            } 
                        ?>
                    </span>    
                </div>
                <div>
                        <label class="form-name" for="phone"> Telefon*: </label>
                        <input type="tel" id="phone" name="phone" class="tel" value="<?php echo $tel ?>" required>
                    <span id="telError">    
                        <?php      
                            if ((count($_POST) > 0) && !$valTel) {
                                echo '<div class="error">Telefon neobsahuje pouze 9 číslic.</div>';
                            } 
                        ?>
                    </span>    
                </div>
                <div>
                        <label class="form-name" for="mail"> Mail*: </label>
                        <input type="email" id="mail" name="mail" value="<?php echo $mail ?>" required>
                    <span id="mailError">    
                        <?php      
                            if ((count($_POST) > 0) && !$valMail) {
                                echo '<div class="error">Chybně zadaný mail.</div>';
                            } 
                        ?>
                    </span>    
                </div>

                <!-- část zabývající se objednávkou -->
                <div>
                    <span class="form-name book-box"> Knížka*: </span>
                    <div class="kniha-box">
                        <div class="kniha-line">
                        <input type="checkbox" name="book[]" id="K1" value="0" <?php echo $knihy[0] ?> >
                        <label for="K1"> Kniha1</label>
                        <input class="kniha <?php echo $hideKnihy[0] ?>" type="number" name="K1value" id="K1value" placeholder="Pocet" value="<?php echo $knihyValue[0]?>">
                        </div>
                        <div class="kniha-line">
                        <input type="checkbox" name="book[]" id="K2" value="1" <?php echo $knihy[1] ?> >
                        <label for="K2"> Kniha2</label>
                        <input class="kniha <?php echo $hideKnihy[1] ?>" type="number" name="K2value" id="K2value" placeholder="Pocet" value="<?php echo $knihyValue[1]?>">
                        </div>
                        <div class="kniha-line">
                        <input type="checkbox" name="book[]" id="K3" value="2" <?php echo $knihy[2] ?> >
                        <label for="K3"> Kniha3</label>
                        <input class="kniha <?php echo $hideKnihy[2] ?>" type="number" name="K3value" id="K3value" placeholder="Pocet" value="<?php echo $knihyValue[2]?>">
                        </div>
                    </div>
                    <span id="knihyError">    
                        <?php      
                            if (count($_POST) > 0) {
                                if (!$oznacenaKniha) {    
                                    echo '<div class="error">Je třeba vybrat alespoň jednu knihu.</div>';
                                }
                                
                                if (!$controlKnihy) {    
                                    echo '<div class="error">Je třeba zadat kladný počet knih.</div>';
                                }         
                            }

                        ?>
                    </span>

                    <!-- ukázka celkové ceny objednávky (spracovává se pomocí javaScriptu {viz. poslední funkce v form_valid.js})-->
                    <div class="controlka-ceny"> 
                        <span > Celková cena objednávky činí: <span id="celkovaCena"><?php echo $cena;?> </span> Kč </span> 
                    </div>
                </div>

                <!-- volba doručení nebo osobní odběr 
                pokud je zvolena doprava je nutné vyplnit adresu-->
                <div>
                        <label for="personal" class="choise">
                            <input type="radio" name="delivery" id="personal" value="p" <?php echo $checkP ?> required>
                            Osobní odběr
                        </label>
                        <label for="transport" class="choise">
                            <input type="radio" name="delivery" id="transport" value="t" <?php echo $checkT ?> required>
                            Doprava
                        </label>
                    <span id="dopravaError"> 
                        <?php      
                            if ((count($_POST) > 0) && !isset($_POST["delivery"])) {
                                echo '<div class="error">Je třeba vybrat jednu z variant.</div>';
                            } 
                        ?>
                    </span>
                </div>
        </fieldset>

        <!-- část s adresou-->
        <fieldset id="address" class="<?php echo $fielAddress ?>">
            <div class="form-table">    
                <div>
                        <label class="form-name" for="address-text" > Adresa*: </label>
                        <input type="text" id="address-text" name="address-text" value="<?php echo $adresa ?>" required>
                    <span id="adresaError">    
                        <?php      
                            if ((count($_POST) > 0) && $checkT && !$valAdresa) {
                                echo '<div class="error">Adresa není ve formátu: název mezera číslo popisné.</div>';
                            } 
                        ?>
                    </span>    
                </div>
                <div>
                        <label class="form-name" for="city"> Město*: </label>
                        <input type="text" id="city" name="city" value="<?php echo $mesto ?>" required>
                    <span id="mestoError">    
                        <?php      
                            if ((count($_POST) > 0) && $checkT && !$valMesto) {
                                echo '<div class="error">Nejedná se o město.</div>';
                            } 
                        ?>
                    </span>    
                </div>
                <div>
                        <label class="form-name" for="zip-code"> PSČ*: </label>
                        <input type="text" id="zip-code" name="zip-code" value="<?php echo $psc ?>" required >
                    <span id="pscError">    
                        <?php      
                            if ((count($_POST) > 0) && $checkT && !$valPsc) {
                                echo '<div class="error">PSČ neobasahuje pouze 5 číslic</div>';
                            } 
                        ?>
                    </span>    
                </div>
            </div>    
        </fieldset>
        
        <!-- Možné napsat poznámku-->
        <fieldset class="form-table">
                <div>
                        <label class="form-name note-label" for="note"> Poznámky: </label>
                        <textarea rows="3" cols="21" id="note"> <?php echo $note ?> </textarea>
                </div>
            <?php 
                //pokud se zapisuje nová podmínka objeví se okénko pro zaškrtnutí podmínek a jinak ne
                if (!$edit) {
                    include("view/agree.php");
                    $submitValue = "Odeslat";
                } else {
                    $submitValue = "Změnit";
                }
            ?>

            <input class="submit" type="submit" value="<?php echo $submitValue; ?>">
        </fieldset>
    </form>
</div>

<script> 
//obsluha zobrazování kolonek na adresu
document.getElementById("personal").addEventListener("click", hide);
document.getElementById("transport").addEventListener("click", show);

//obsluha zobrazování kolonek na počet chtěných knih
document.getElementById("K3").addEventListener("click", function(){proved("K3")});
document.getElementById("K2").addEventListener("click", function(){proved("K2")});
document.getElementById("K1").addEventListener("click", function(){proved("K1")});

//zobrazí kolonky na adresu
function hide() {
    document.getElementsByTagName("fieldset")[1].setAttribute("class", "hide");
    document.getElementById("address-text").value = "a 1";
    document.getElementById("city").value = "aa";
    document.getElementById("zip-code").value = "99999";
}

//schová kolonky na adresu
function show() {
    document.getElementsByTagName("fieldset")[1].removeAttribute("class", "hide");
    if (document.getElementById("address-text").value == "a 1") {
        document.getElementById("address-text").value = "";
        document.getElementById("city").value = "";
        document.getElementById("zip-code").value = "";
    }
}

//zobrazí kolonku na počet
function schovej(id) {
    document.getElementById(id).setAttribute("class", "hide");
}

//schová kolonku na počet
function ukaz(id) {
    document.getElementById(id).removeAttribute("class", "hide");
    document.getElementById(id).setAttribute("class", "kniha");
}

//rozhodne zda se objeví nebo schová kolonka na počet 
function proved(i) {
    var click = document.getElementById(i).checked;
    var id = i + "value";
    if (click) {
        ukaz(id);
    } else {
        schovej(id);
    }
}
<?php 
//pokud se opravuje není potřeba zaklikávat souhlasení s podmínkami
if(!$edit) {
    echo 'document.getElementById("agree").addEventListener("click", function() {uzDobre("agreeJs")});';
}

//kontrola formuláře pomocí javaScriptu
include("js/form_valid.js");
?>
</script>