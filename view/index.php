<!-- vizuální stránka úvodní stránky --> 
<div class="content in-content">
    
    <!-- výpis hlášek na této stránce ze serveru, 
    když neco delá a odkazuje ve výsledku sem --> 
    <div class="<?php echo $class?>"> 
                <?php echo $message; ?>
    </div>

    <header class="form-header">
        <h1>Kout historických kuriozit</h1>
    </header>
    <p class="uvod-text">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Nemo enim ipsam voluptatem quia voluptas 
    sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi 
    nesciunt. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. 
    Proin in tellus sit amet nibh dignissim sagittis. Sed elit dui, pellentesque a, faucibus vel, interdum 
    nec, diam. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Fusce 
    aliquam vestibulum ipsum. Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil 
    molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur? Fusce wisi. Etiam quis 
    quam.</p>

    <!-- výčet produktů --> 
    <div class="title">
        <h2> Produkty</h2>
    </div> 

    <!-- produkt 1--> 
    <div class="box box1">
        <div class="box-header">
            <h3 class="box-name">Kniha1</h3>
        </div>
        <div class="box-body box-knihy">
            <img class="kniha-picture" src="pictures/empty-image.png" alt="kniha1"> 
            <div class="box-text-knihy"> Text Text Text Text Text Text Text Text
                Text Text Text Text Text Text Text Text Text Text Text Text
                Text Text Text Text Text Text Text Text
            </div> 
            <div class="box-text-knihy"> Cena: 230kč
            </div> 
        </div>
        <div class="box-footer">
            <a href="order_form.php?book=0" class="box-button but1">Objednat</a>
        </div>    
    </div>

    <!-- produkt 2--> 
    <div class="box box1">
        <div class="box-header">
            <h3 class="box-name">Kniha2</h3>
        </div>
        <div class="box-body box-knihy">
            <img class="kniha-picture" src="pictures/empty-image.png" alt="kniha2">  
            <div class="box-text-knihy">
                Text Text Text Text Text Text Text Text
                Text Text Text Text Text Text Text Text Text Text Text Text
                Text Text Text Text Text Text Text Text
            </div>
            <div class="box-text-knihy"> Cena: 200kč
            </div> 
        </div>
        <div class="box-footer">
            <a href="order_form.php?book=1" class="box-button but1">Objednat</a>
        </div>
    </div>

    <!-- produkt 3--> 
    <div class="box box1">
        <div class="box-header">
            <h3 class="box-name">Kniha3</h3>
        </div>
        <div class="box-body box-knihy">
            <img class="kniha-picture" src="pictures/empty-image.png" alt="kniha3"> 
            <div class="box-text-knihy">
                Text Text Text Text Text Text Text Text
                Text Text Text Text Text Text Text Text Text Text Text Text
                Text Text Text Text Text Text Text Text
            </div>
            <div class="box-text-knihy"> Cena: 150kč
            </div> 
        </div>
        <div class="box-footer">
            <a href="order_form.php?book=2" class="box-button but1">Objednat</a>
        </div>
    </div>

    <!-- zde budou nejnovější články -->
    <div class="title">
        <h2>Články</h2>
    </div>
    <div class="clanky">
        
        <!-- článek 1-->
        <div class="box mar1">
            <div class="box-header">
                <h3 class="box-name">Article1</h3>
            </div>
            <div class="box-body box-arcicle">
                <p class="box-text">
                    Text Text Text Text Text Text Text Text
                    Text Text Text Text Text Text Text Text Text Text Text Text
                    Text Text Text Text Text Text Text Text...
                </p>
            </div>
            <div class="box-footer">
                <a href="list_article.php#1" class="box-button but2">Číst více</a>
            </div>
        </div>

        <!-- článek 2-->
        <div class="box box2">
            <div class="box-header">
                <h3 class="box-name">Article2</h3>
            </div>
            <div class="box-body box-arcicle">
                <p class="box-text">
                    Text Text Text Text Text Text Text Text
                    Text Text Text Text Text Text Text Text Text Text Text Text
                    Text Text Text Text Text Text Text Text...
                </p>
            </div>
            <div class="box-footer">
                <a href="list_article.php#2"  class="box-button but2">Číst více</a>
            </div>
        </div>

        <!-- článek 3-->
        <div class="box box2">
            <div class="box-header">
                <h3 class="box-name">Article3</h3>
            </div>
            <div class="box-body box-arcicle">
                <p class="box-text">
                    Text Text Text Text Text Text Text Text
                    Text Text Text Text Text Text Text Text Text Text Text Text
                    Text Text Text Text Text Text Text Text...
                </p>
            </div>
            <div class="box-footer">
                <a href="list_article.php#3" class="box-button but2">Číst více</a>
            </div>
        </div>
        
        <!-- odkaz na knihovnu článků-->
        <a href="list_article.php" class="more more-button"> Více </a>  
    </div> 
</div>  