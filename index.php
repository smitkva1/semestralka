<?php 
    include("helper/ses.php");

    /*
    zde se obsluhuje úvodní stránka
    */
    include("helper/functions.php");

    //pokud se chtěl uživatel odhlásit
    if (isset($_GET["logout"])) {
        $logout = osetri($_GET["logout"]);

        if ($logout) {
            $_SESSION["login"] = FAlSE;
            unset($_SESSION["odPoctu"]);
        }
    }

    //zpravování hlášky ze serveru
    if (isset($_SESSION["message"])) {
        $message = $_SESSION["message"];
        unset($_SESSION["message"]);
    } else {
        $message = "";
    }

    //zahlavi
    include("htmlKomponenty/zahlavi.php");

    //telo
    include("view/index.php");

    //zapatí
    include("htmlKomponenty/zapati.php"); 
?>