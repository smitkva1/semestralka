<?php 
include("helper/ses.php");

/*
Zde se vykonávají všechny potřebné úkony pro obsluhu stránky,
kde se vypisují objednávky do tabulky
*/
if ($_SESSION["login"]) {
    include("helper/functions.php");
    
    //přípravy zprávy pro vyvolání
    if (isset($_SESSION["message"])) {
        $message = $_SESSION["message"];
        unset($_SESSION["message"]);
    } else {
        $message = "";
    }
    
    //zjištění kolik bude výpisů na stránku
    if (isset($_COOKIE["number"])) {
        $naStranku = $_COOKIE["number"];
    } else {
        $naStranku = 10;
    }

    //pokud bylo zavoláno refresh
    if (isset($_GET["refresh"]) && isset($_SESSION["prikaz"])) {
        unset($_SESSION["prikaz"]);
        $refresh = TRUE;
    } else {
        $refresh = FALSE; 
    }

    /*zjištění od jaké položky se bude daná stránka vypisovat 
    neboli na které stránce jsme*/
    if (!isset($_SESSION["odPoctu"])) {
        $_SESSION["odPoctu"] = 0;
    }
    
    //připojení k databázi pro možnost vypsání objednávek
    define ("DB_HOST", "localhost");
    define ("DB_NAME", "semestralka");
    define ("DB_USER", "erakles");
    define ("DB_PASSWD", "NXUBpjGu1Taj00qo");
    
    
    // pokusim se pripojit k DB stroji
    $link = mysqli_connect(DB_HOST, DB_USER, DB_PASSWD);
    if (!$link) {
    echo "Nepodařilo se spojit s DB.<br>";
    echo mysqli_connect_error();
    exit();
    }
    
    // pokusim se vybrat si spravnou databazi
    $success = mysqli_select_db($link, DB_NAME);
    if (!$success) {
    echo "Nepodařilo se přepnout na správnou databázi";
    exit();
    }

    //pokud bylo zavoláno delete nějaké objednávky
    if (isset($_GET["delete"])) {
        $id = osetri($_GET["delete"]);

        if (is_numeric($id)) {
            $id = (string) $id;
            $sql = "DELETE FROM objednavky WHERE order_id=".$id;
            $result = mysqli_query($link, $sql);

            if ($result) {
                $message  = "Úspěšně odebráno";
            } else {
                $message  = "Operace se nezdařila";
            }
        }
    }

    //pokud se bude nějaká objednávka upravovat
    if (isset($_GET["edit"])) {
        $id = osetri($_GET["edit"]);

        if (is_numeric($id)) {
            $id = (string) $id;
            $sql = "SELECT * FROM objednavky WHERE order_id=".$id;
            $result = mysqli_query($link, $sql);

            if ($result) {
                $row = mysqli_fetch_assoc($result);
                $_SESSION["row"] = $row;
                header('Location: order_form.php');
            }  else {
                $message  = "Operace se nezdařila";
            }
        }
    }

    //spracování filtru
    $jmeno = "";
    $prijmeni = "";
    $telefon = "";
    $mail = "";
    $dopravaO = "";
    $dopravaD = "";
    $poznamkaJ = "";
    $poznamkaN = "";
    $doprava = "";
    $od = "";
    $do = "";
    
    $prikaz = "";
    if (count($_POST) > 0) {
        
        if (isset($_POST["jmeno"])){
            $jmeno = osetri($_POST["jmeno"]);
            $jmeno = (string) $jmeno;
            if ($jmeno) { 
            $prikaz = " WHERE jmeno = '".$jmeno."'";
            }  
        }
        if (isset($_POST["prijmeni"])){
            $prijmeni = osetri($_POST["prijmeni"]);
            if ($prijmeni) {
                if ($prikaz) {
                    $prikaz =$prikaz." AND prijmeni = '".$prijmeni."'";
                } else {
                    $prikaz = " WHERE prijmeni = '".$prijmeni."'";
                }
            }
        }
        if (isset($_POST["telefon"])){
            $telefon = osetri($_POST["telefon"]);
            if ($telefon) {
                if ($prikaz) {
                    $prikaz =$prikaz." AND telefon = '".$telefon."'";
                } else {
                    $prikaz = " WHERE telefon = '".$telefon."'";
                }
            }
        }
        if (isset($_POST["mail"])){
            $mail = osetri($_POST["mail"]);
            if ($mail) {
                if ($prikaz) {
                    $prikaz =$prikaz." AND mail = '".$mail."'";
                } else {
                    $prikaz = " WHERE mail = '".$mail."'";
                }
            }
        }
        if (isset($_POST["doprava"])){
            $doprava = osetri($_POST["doprava"]);
            if ($doprava == 1) {
                $dopravaO = "";
                $dopravaD = "selected";
            }elseif ($doprava == 0) {
                $dopravaO = "selected";
                $dopravaD = "";
            } 
            if ($doprava != -1) {
                if ($prikaz) {
                    $prikaz =$prikaz." AND doprava = '".$doprava."'";
                } else {
                    $prikaz = " WHERE doprava = '".$doprava."'";
                }
            }
        }
        if (isset($_POST["poznamka"])){
            $poznamka = osetri($_POST["poznamka"]);
            if ($poznamka == 1) {
                $poznamkaJ = "selected";
                $poznamkaN = "";
                $note = "<>";
            } elseif ($poznamka == 0)  {
                $poznamkaJ = "";
                $poznamkaN = "selected";
                $note = "=";
            }
            if ($poznamka != -1) {
                if ($prikaz) {
                    $prikaz =$prikaz." AND poznamka ".$note." ''";
                } else {
                    $prikaz = " WHERE poznamka ".$note." ''";
                }
            }
        }
        if (isset($_POST["od"])){
            $od = osetri($_POST["od"]);
            $od = (string) $od;
            if ($od) {
                if ($prikaz) {
                    $prikaz =$prikaz." AND datum >= '".$od." 00:00:00'";
                } else {
                    $prikaz = " WHERE datum >= '".$od." 00:00:00'";
                }
            } 
        }
        if (isset($_POST["do"])){
            $do = osetri($_POST["do"]);
            $do = (string) $do;
            if ($do) {
                if ($prikaz) {
                    $prikaz =$prikaz." AND datum <= '".$od." 00:00:00'";
                } else {
                    $prikaz = " WHERE datum <= '".$od." 00:00:00'";
                }
            } 
        }
    }
    
    //uprava příkazu 
    if (isset($_SESSION["prikaz"]) && $prikaz == "") {
        $prikaz = $_SESSION["prikaz"];
    } elseif  (!$refresh && $prikaz)  {
        $_SESSION["prikaz"] = $prikaz;
        $refresh = TRUE;
        unset($_SESSION["odPoctu"]);
    }

    //zjištění celkového počtu objednávek
    $sql = "SELECT * FROM objednavky ".$prikaz;
    $result = mysqli_query($link, $sql);
    if ($result) {
        $count = 0;
        while($row = mysqli_fetch_assoc($result)) {
            $count += 1;
        }
        mysqli_free_result($result);
    }

    //vyhodnocení příkazu ze směrovací lišty ohledně stránkování
    if (isset($_GET["list"])) {
        $zadost = osetri($_GET["list"]);
        $puvodni = (isset($_SESSION["odPoctu"])) ? $_SESSION["odPoctu"] : 0;
        $posledni = $count - ($count % $naStranku);
        
        if ($zadost == "first") {
            $odPoctu = 0;
            $_SESSION["odPoctu"] = $odPoctu;  
        } elseif ($zadost == "last") {
            $odPoctu = $posledni;
            $_SESSION["odPoctu"] = $odPoctu;  
        } elseif ($zadost == "back") {
            $nove = $puvodni - $naStranku;
            $odPoctu = ($nove < 0) ? 0 : $nove;
            $_SESSION["odPoctu"] = $odPoctu;  
        } elseif ($zadost == "next") {
            $nove = $puvodni + $naStranku;
            $odPoctu = ($nove > $posledni) ? $posledni: $nove;
            $_SESSION["odPoctu"] = $odPoctu;  
        } elseif (is_numeric($zadost)) {
                $zadost = intval($zadost-1);
                $nove = $zadost * $naStranku;
                if ($nove < 0 || $nove > $posledni) {
                $odPoctu = $puvodni;
                } else {
                    $odPoctu = $nove;
                    $_SESSION["odPoctu"] = $odPoctu;
                }     
        } else {
            $odPoctu = $puvodni;
            $_SESSION["message"] = "Nastala chyba při stránkování.";
            $_SESSION["odPoctu"] = $odPoctu;  
        }   
    } else {
        //pokud nic tak zůstane kde byl
        $odPoctu = (isset($_SESSION["odPoctu"])) ? $_SESSION["odPoctu"] : 0;
    }

    //pokud se zavolalo řazení
    if (isset($_GET["sortUp"]) || isset($_GET["sortDown"])) {
        $sql = sqlPrikazVytvor($_GET, $prikaz);
    } else {
        //jinak poslendní objednávka na první pozici 
        $sql = "SELECT * FROM objednavky ".$prikaz." ORDER BY order_id DESC";
    }
    
    //obohacení požadavku na databázi na základě stránkování
    $sql = $sql ." LIMIT ".$naStranku." OFFSET ".$odPoctu;
    $result = mysqli_query($link, $sql);
    
    //zjistí skin kvůli administrátorskému menu
    $skin = zjistiSkin();
    
    //zahlaví
    include("htmlKomponenty/zahlavi.php");
    if ($skin == "style1") {
        //admin lišta
        include("htmlKomponenty/admin_lista_style1.html");   
    }
    //tělo
    include("view/manager.php");
    //zápatí
    include("htmlKomponenty/zapati.php");

    mysqli_close($link);
} else {
    //pokud nejsi přihlášen nastav hlášku a jdi na úvodní stránku
    $_SESSION["message"] = "Nejprve je třeba se přihlásit";
    header('Location: index.php');
}
?>