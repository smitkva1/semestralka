﻿<?php    
    include("helper/ses.php");
    /*
    zde je stránka která obsluhuje formulář
    kontroluje se odsud správnost a uchovávají se zde hodnoty dokud se neodešlou a 
    nepřejde na jinou stránku
    */
    include("helper/form_validation.php");
    include("helper/functions.php");
    
    $cena = 0;
    //předefinování proměných s podmínkou zda se bude upravovat nebo vytvářet
    if (isset($_SESSION["row"])) {
        $row = $_SESSION["row"];
        $edit = TRUE;
    
        $id = (string) $row["order_id"];

        $jmeno = $row["jmeno"];
        $prijmeni = $row["prijmeni"];
        $tel = $row["telefon"];
        $mail = $row["mail"];
        $cena = $row["celkovacena"];
        $note = $row["poznamka"]; 

        $checkP = ($row["doprava"] == 0) ? "checked" : "";
        $checkT = ($row["doprava"] == 1) ? "checked" : "";

        if ($checkT) {
            $adresa = $row["adresa"];
            $mesto = $row["mesto"];
            $psc = $row["psc"];
        } 

        $fielAddress = ($row["doprava"] == 0) ? "hide" : "";
        $oznacenaKniha = "";
        $knihy = ["", "", ""];
        $hideKnihy = ["hide", "hide", "hide"];
        
        $knihyValue = [$row["kniha1"], $row["kniha2"], $row["kniha3"]];
        $controlKnihy = TRUE;
        
        if ($row["kniha1"] > 0) {
            $knihy[0] = "checked";
            $hideKnihy[0] = "";
        }  
        if ($row["kniha2"] > 0) {
            $knihy[1] = "checked";
            $hideKnihy[1] = "";
        }
        if ($row["kniha3"] > 0) {
            $knihy[2] = "checked";
            $hideKnihy[2] = "";
        }
    
    } else {
        $edit = FALSE;

        $jmeno = "";
        $prijmeni = "";
        $tel = "";
        $mail = "";
        
        $knihyValue = ["", "", ""];
        $controlKnihy = TRUE;

        $adresa = "a 1";
        $mesto = "aa";
        $psc = "99999";
        $note = ""; 

        $checkP = "";
        $checkT = "";

        $fielAddress = "hide";
        $oznacenaKniha = "";
        $knihy = ["", "", ""];
        $hideKnihy = ["hide", "hide", "hide"];
    }
        $token = md5(time());
        $valAdresa = "";
        $valMesto = "";
        $valPsc = "";

    //zpracování pokud byla objednána nějaká kniha
    if (isset($_GET["book"])) {
        $kniha = osetri($_GET["book"]);
        if (is_numeric($kniha)) {
            $knihy[$kniha] = "checked";
            $hideKnihy[$kniha] = "";
            $knihyValue[$kniha] = 1;
        }
    }

    /*
    kontrola formuláře pokud je odeslaný
    */
    if (count($_POST) > 0) {
        $jmeno = osetri($_POST["name"]);
        $prijmeni = osetri($_POST["surname"]);
        $tel = osetri($_POST["phone"]);
        $mail = osetri($_POST["mail"]);

        if (isset($_POST["note"])) { 
           $note = osetri($_POST["note"]); 
        } ;

        //kontrola knih
        if (isset($_POST["book"])) {
            $oznacenaKniha = TRUE;
            $knihy = vyresKnihy($_POST["book"]);
        }
        if ($knihy[0]){
            $knihyValue[0] = osetri($_POST["K1value"]);
            $hideKnihy[0] = "";
            $valK1 = vyresK($knihyValue[0]); 
            if (!$valK1) {
                $controlKnihy = FALSE;
            }
        }
        if ($knihy[1]){
            $knihyValue[1] = osetri($_POST["K2value"]);
            $hideKnihy[1] = "";
            $valK2 = vyresK($knihyValue[1]); 
            if (!$valK2) {
                $controlKnihy = FALSE;
            }
        }
        if ($knihy[2]){
            $knihyValue[2] = osetri($_POST["K3value"]);
            $hideKnihy[2] = "";
            $valK3 = vyresK($knihyValue[2]); 
            if (!$valK3) {
                $controlKnihy = FALSE;
            }
        }
        //kontrola jména, příjmení, telefonu a mailu
        $valJmeno = valName($jmeno);
        $valPrijmeni= valSurname($prijmeni);
        $valTel = valTel($tel);
        $valMail = valMail($mail);
            
        $valFirstPart = $valJmeno && $valPrijmeni && $valTel && $valMail && $oznacenaKniha && $controlKnihy;

        /*
        kontrola adresy pokud je označená a je označeno doprava
        */
        if (isset($_POST["delivery"])) {
            if ($_POST["delivery"] == "t") {
                $checkT = "checked";
               
                $adresa = osetri($_POST["address-text"]);
                $mesto = osetri($_POST["city"]);
                $psc = osetri($_POST["zip-code"]);
                    
                $valAdresa = valAddress($adresa);
                $valMesto = valCity($mesto);
                $valPsc = valZip($psc);
                    
                $valWholeAddress = $valAdresa && $valMesto && $valPsc;
                    
                if ($valWholeAddress) {
                    $adresaOk = TRUE;

                } else {
                    $adresaOk = FALSE;
                }

            } else {
                $checkP = "checked";                   
                $adresa = "";
                $mesto = "";
                $psc = "";                 
                $adresaOk = TRUE;
                }
            
            if ($checkP) {
                $fielAddress = "hide";
            } else {
                $fielAddress = "show";
            }
            
            $isAgree = (isset($_POST["agree"]) || $edit) ? TRUE : FALSE;

            if ($valFirstPart && $adresaOk && $isAgree) {
                
                //okontroované hodnoty se pošlou do databáze
                include("database/order.php");

                //na základě toho zda se opravovalo nebo vytvářelo dojde k přesměrování
                if (isset($_SESSION["row"])) {
                    $_SESSION["message"] = "Úspěsně nahrazeno";
                    unset($_SESSION["row"]);
                    header('Location: control_manager.php'); 
                } else {
                    header('Location: end.php');
                }                
            }   
        }   
    }
    
    //zahlaví
    include("htmlKomponenty/zahlavi.php");
    //tělo
    include("view/objednavky.php");
    //zapatí
    include("htmlKomponenty/zapati.php"); 
?>