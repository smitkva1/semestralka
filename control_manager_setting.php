<?php 
include("helper/ses.php");
/* 
Zde je jsou veškeré obhospodařující funkce stránky nastavní v administrátorské sekci
*/

if ($_SESSION["login"]) {
    include("helper/functions.php");
    
    //přípravy zprávy pro vyvolání
    if (isset($_SESSION["message"])) {
        $message = $_SESSION["message"];
        unset($_SESSION["message"]);
    } else {
        $message = "";
    }
    
    //vyhodnocení odeslaných formulářů
    $valNumber = TRUE;
    $valNumberA = TRUE;
    $number = "";
    if (isset($_GET)) {
        
        //vyhodnocení skinu a nastaví cookie na jeden den
        if (isset($_GET["skin"])){
            $skin = osetri($_GET["skin"]);
            setcookie("skin", $skin, time() + (86400 * 30), "/");
            $_SESSION["message"] = "Úspěšně změněn skin.";
            header('Location: control_manager_setting.php');
        }

        //vyhodnocení poctu řádků v tabulce s objednávkama a nastaví cookie na jeden den
        if (isset($_GET["number"])){
            $number = osetri($_GET["number"]);
            $isNumber = is_numeric($number) && $number > 0;
            if ($isNumber) {    
                setcookie("number", $number, time() + (86400 * 30), "/");
                $_SESSION["message"] = "Úspěšně změněn počet objednávek v tabulce";
                unset($_SESSION["odpoctu"]);
                header('Location: control_manager_setting.php');
            } else {
                $valNumber = FALSE;
            }
        }
    }

    $skin = zjistiSkin();

    //zahlaví
    include("htmlKomponenty/zahlavi.php");
    if ($skin == "style1") {
        //admin lišta pokud je nastavven styl 1
        include("htmlKomponenty/admin_lista_style1.html");   
    }
    //tělo
    include("view/manager_setting.php");
    //zápatí
    include("htmlKomponenty/zapati.php");
} else {
    //pokud nejsi přihlášen nastav hlášku a jdi na úvodní stránku
    $_SESSION["message"] = "Nejprve je třeba se přihlásit";
    header('Location: index.php');
}

  
?>